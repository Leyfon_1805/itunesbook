angular.module('app')
  .controller('TabBodyCtrl', TabBodyCtrl);

/** @ngInject */
function TabBodyCtrl(itunesAPIService, $state) {
  var ctrl = this;
  ctrl.content = {};
  ctrl.author = $state.current.name;
  loadData();

  function loadData() {
    itunesAPIService.search(ctrl.author).then(function (response) {
      ctrl.content = response.data;
    })

  }
}
