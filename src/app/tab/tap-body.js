angular
  .module('app')
  .component('tabBody', {
    templateUrl: 'app/tab/tab-body.html',
    controller: 'TabBodyCtrl as ctrl'
  });
