angular.module('app')
  .service('itunesAPIService', itunesAPIService);

/** @ngInject */
function itunesAPIService($http) {
  var service = {
    search: search
  };

  function search(author){
    return $http({
      method: 'GET',
      url: '/mocks/'+ author +'.json'
    });
  }
 return service;
}
