angular
  .module('app')
  .config(routesConfig);

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/Hemingway');

  $stateProvider
    .state('Hemingway', {
      url: '/Hemingway',
      templateUrl: 'app/main.html'
    })
    .state('Dickens', {
      url: '/Dickens',
      templateUrl: 'app/main.html'
    })
    .state('Shakespeare', {
      url: '/Shakespeare',
      templateUrl: 'app/main.html'
    })
  ;
}
